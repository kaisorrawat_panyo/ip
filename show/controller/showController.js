var ipModule = angular.module('IP', []);

ipModule.controller('ipController', function($scope, $http) {

    

    $scope.addIP = function(ipCom) {
        

        var url = "https://freegeoip.net/json/" + ipCom;
        
        alert(url);
        $http({
                url: url,
                method: "GET"
               
            })
            .success(function(data) {

                $scope.testIP = data.ip;
                $scope.testCountry_code = data.country_code;
                $scope.testCountry_name = data.country_name;
                $scope.testRegion_code = data.region_code;
                $scope.testRegion_name = data.region_name;
                $scope.testCity = data.city;
                $scope.testZip_code = data.zip_code;
                $scope.testTime_zone = data.time_zone;
                $scope.testLatitude = data.latitude;
                $scope.testLongitude = data.longitude;
                $scope.testMetro_code = data.metro_code;
                

            })
            .error(function(data) {
                
            })


    };
});